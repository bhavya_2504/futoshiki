\documentclass[10pt,aspectratio=169]{beamer}
% \mode<presentation>{}

%\usepackage{media9}
% \usepackage{amssymb,amsmath,amsthm,enumerate}
%\usepackage[brazil]{babel}
\usepackage[utf8]{inputenc}
\usepackage{array}
\usepackage[parfill]{parskip}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{bm}
\usepackage{amsfonts,amscd}
\usepackage[]{units}
\usepackage{listings}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{tcolorbox}
\usepackage{physics}
\usepackage[T1]{fontenc}

\usenavigationsymbolstemplate{}

% Enable colored hyperlinks
\hypersetup{colorlinks=true}

% The following three lines are for crossmarks & checkmarks
%\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

% Numbered captions of tables, pictures, etc.
\setbeamertemplate{caption}[numbered]

%\usepackage[superscript,biblabel]{cite}
\usepackage{algorithm2e}
\renewcommand{\thealgocf}{}

% Bibliography settings
\usepackage[style=ieee]{biblatex}
\setbeamertemplate{bibliography item}{\insertbiblabel}
\addbibresource{references.bib}

% Glossary entries
\usepackage[acronym]{glossaries}
\newacronym{ML}{ML}{machine learning}
\newacronym{HRI}{HRI}{human-robot interactions}
\newacronym{RNN}{RNN}{Recurrent Neural Network}
\newacronym{LSTM}{LSTM}{Long Short-Term Memory}


\theoremstyle{remark}
\newtheorem*{remark}{Remark}
\theoremstyle{definition}

\newcommand{\empy}[1]{{\color{darkorange}\emph{#1}}}
\newcommand{\empr}[1]{{\color{cardinalred}\emph{#1}}}
\newcommand{\examplebox}[2]{
\begin{tcolorbox}[colframe=darkcardinal,colback=boxgray,title=#1]
#2
\end{tcolorbox}}

\usetheme{Stanford}
% \input{./style_files_stanford/my_beamer_defs.sty}
%  \logo{\includegraphics[height=0.40in]{Imagens/Logo LAPID2.png}}

% commands to relax beamer and subfig conflicts
% see here: https://tex.stackexchange.com/questions/426088/texlive-pretest-2018-beamer-and-subfig-collide
% \makeatletter
% \let\@@magyar@captionfix\relax
% \makeatother

%-------------------------TITULO/SUBTITULO DO PROJETO------------------------------------

\title[\textcolor{white}{Team Futoshiki}]{FUTOSHIKI}


%----------------------------------------------------------------------------------------

%\beamertemplatenavigationsymbolsempty

\begin{document}
%-----------------------INFORMAÇOES DO PROJETO---------------------------------------
\author[FUTOSHIKI]{
\begin{tabular}{c}
\Large
TEAM 14\\
Deepika, Ameesha, Bhavya, Likhitha, Pradeepthi
%-----------------------ADICIONAR E-MAIL---------------------------------------
    % \footnotesize \href{mailto:authoremail@stanford.edu}{authoremail@stanford.edu}
\end{tabular}
\vspace{-4ex}}
%---------------------------LOGO DA


\begin{noheadline}
\begin{frame}\maketitle\end{frame}
\end{noheadline}

\setbeamertemplate{itemize items}[default]
\setbeamertemplate{itemize subitem}[circle]

%------------------------------------------------------------------------
\section{Overview}
\begin{frame}[fragile]{Overview}
\Large
% \begin{block}{dataset\_ipe.xlsx}
\beginblock\textbf{FUTOSHIKI}
    \begin{description}
   \item[$\bullet$~] Futoshiki is a  board based puzzle game also known as under the name \textbf{Unequal}.\\
   \item[$\bullet$~] It is playable on a square board having a given fixed size.\\
    \end{description}
\end{frame}
%--------------------------------------------------------------------

\section{Análise do Dataset}
\begin{frame}[fragile]{Rules}
\Large
% \begin{block}{dataset\_ipe.xlsx}
   \begin{description}
   \item[$\bullet$~] Its a puzzle containing N * N grid.
   \item[$\bullet$~] And some inequalities like '<', '>' are placed in between the cells of the grid.
   \item[$\bullet$~] We have to fill the cells of the grid with numbers ranging in between 1 to N, such that the inequality constraints must be satisfied.
\end{description}
\end{frame}

%--------------------------------------------------------------------
\section{Análise do Dataset}
\begin{frame}[fragile]{Objectives}
\Large
% \begin{block}{dataset\_ipe.xlsx}
   \begin{description}
   \item[$\bullet$~] Our purpose was to  implement a puzzle that is a little tricky to solve such that user find it's interesting.
   \item[$\bullet$~] We have provided user 3 different levels of the game based on individual's interest one can select the difficulty level and play.
\end{description}
\end{frame}


%---------------------------------------------------------------
\section{Filtragem das Espécies}
\begin{frame}[fragile]{Technology Stacks}
\Large
% \begin{block}{dataset\_ipe.xlsx}
\begin{description}
    \centering
    \includegraphics[ width=2.4cm]{python_logo.png}
%\end{left}
%\begin{right}
     \centering
     \includegraphics[width=2.4cm]{vs_logo.png}
%\end{right}
%\begin{right}
     \centering
     \includegraphics[width=2.4cm]{latex_logo.png}
%\end{right}
%\begin{right}
     \centering
     \includegraphics[width=2.4cm]{git_logo.png}
\end{description}
% \end{block}
\end{frame}
%---------------------------------------------------------------
\begin{frame}[fragile]{Project Flow}
\Large
\begin{description}
   \item[$\bullet$~] \textbf{Day 1} : Going  through the description and analysing briefly about the game.\\
   Dividing our tasks so that everyone will be working on something.\\
   Creating our project repository on Gitlab.
   \item[$\bullet$~] \textbf{Day 2} : Designing the basic layout of the game.\\
                          Generating a basic 4 * 4 puzzle in VsCode.\\
                          Learning Git commands to push our work into gitlab.\\
                          Working on logic implementation of the puzzle.
   \item[$\bullet$~] \textbf{Day 3 } : Implementing the logic of the game.\\
   Learning Latex for our final presentation of the project.\\
   Executing the code and debugging if there are any errors.
   
   
   
\end{description}
\end{frame}
%--------------------------------------------------------------------
\begin{frame}[fragile]{Project Flow}
\Large
\begin{description}
   \item[$\bullet$~] \textbf{Day 4} : Working on implementing the logic if there are any errors.\\
   Working Implementing easy, medium and hard levels of the puzzle.
   Pushing the code to Gitlab.
   Working on the changes if needed.
   Preparing for our Final presentation of the project.
   \item[$\bullet$~] \textbf{Day 5} : Submitting our final codes and presenting our project to everyone.
   Bugs are also checked for the last time.
\end{description}
\end{frame}
%----------------------------------------------------------------------\section{Filtragem das Espécies}
\begin{frame}[fragile]{Day 1}
\Large
\beginblock{Created project repository in gitlab and updated readme.}
     
% \begin{block}{dataset\_ipe.xlsx}
\begin{figure}
    \centering
    \includegraphics[ width=10.5cm]{ss.p}
\end{figure}
\end{frame}
%----------------------------------------------------------------------
\begin{frame}[fragile]{Day 2}
\Large
\beginblock{Generated basic puzzle.}
% \begin{block}{dataset\_ipe.xlsx}
\begin{figure}
    \centering
    \includegraphics[ width=10.5cm]{puz.g}
\end{figure}
\end{frame}
%----------------------------------------------------------------------
\begin{frame}[fragile]{Day 3}
\Large
\beginblock{Implemented the logic for puzzle.}
% \begin{block}{dataset\_ipe.xlsx}
\begin{figure}
    \centering
    \includegraphics[ width=10.5cm]{logic.g}
\end{figure}
\end{frame}
%----------------------------------------------------------------------
\begin{frame}[fragile]{Day 4}
\Large
\beginblock{Implemented logic for easy, medium and hard puzzles.}
% \begin{block}{dataset\_ipe.xlsx}
\begin{figure}
    \centering
    \includegraphics[ width=10.5cm]{final.g}
\end{figure}
\end{frame}
%----------------------------------------------------------------------
\begin{frame}[fragile]{Day 5}
\Large
\beginblock{Done with our project.}
% \begin{block}{dataset\_ipe.xlsx}
\begin{figure}
    \centering
    \includegraphics[ width=5.5cm]{futo.g}
\end{figure}
\end{frame}
%------------------------------------------------------------------------------

\begin{frame}[fragile]{Our Project}
\Large
\centering
\begin{center}
   
       \item \textbf{DEMO}
   \end{center}
\end{frame}

%----------------------------------------------------------------------
\section{Matriz de Correlação}
\begin{frame}[fragile]{Edge cases}
\Large
\begin{description}
   \item[$\bullet$~] We tried to cover the edge case of wrong input number.If user enters a value greater than N we will say enter value between the range 1 to N.
   \item[$\bullet$~] We also tried to cover the edge case of repeated number.If the user enters a value twice either in same row or column we will give an alert.
\end{description}
\end{frame}

%--------------------------------------------------------------------


\begin{frame}[fragile]{Learnings}
\Large
\begin{description}
   \item[$\bullet$~] We learned to work in a team and how to collaborate with different people having different mindset on the same thing.
   \item[$\bullet$~] We had no knowledge of version control systems like git.We learned how to use different Open Source softwares like Git lab and Latex.  
   \item[$\bullet$~] We have more knowledge in python than before and explored more concepts in python.
\end{description}
\end{frame}

%--------------------------------------------------------------------


\begin{frame}[fragile]{Challenges}
\Large
\begin{description}
       \item[$\bullet$~] We found it very difficult to implement the logic as it is a very tricky case.
       \item[$\bullet$~] We were very new to gitlab and latex.
       \item[$\bullet$~] Working on different levels of the games individually and found it very difficult to merge the code together.
   \end{description}
\end{frame}

%--------------------------------------------------------------------
% \section{Matriz de Correlação}
\begin{frame}[fragile]{Future Scope}
\Large
\begin{description}
       \item[$\bullet$~] Increase the complexity of puzzle by adding more levels.
       \item[$\bullet$~]  Developing the puzzle as a game using web development.
       \item[$\bullet$~] Making it more interesting by introducing timer and lives for each level.
   \end{description}
\end{frame}
%---------------------------------------------------------------------
\begin{frame}[fragile]{Statistics}
\Large
\beginblock
% \begin{block}{dataset\_ipe.xlsx}
\begin{figure}
    \centering
    \includegraphics[ width=10.5cm]{futo.git}
\end{figure}
\end{frame}
%---------------------------------------------------------------------
\section{Matriz de Correlação}
\begin{frame}[fragile]{Our Project}
\Large

\begin{description}
       \item[$\bullet$~]  \textbf{GitLab}:\\
       https://gitlab.com/bhavya_2504/futoshiki\\
       \item[$\bullet$~]  \textbf{References}:\\ https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html\\ Python Documentation.\\
       \\Youtube, W3 Schools, TutorialsPoint.
\end{description}
\end{frame}
%----------------------------------------------------------------------
\section{Matriz de Correlação}
\begin{frame}[fragile]{Our Project}
\Large
\centering
\begin{center}
   
       \item \textbf{Thank You!}
   \end{center}
\end{frame}
\end{document}

